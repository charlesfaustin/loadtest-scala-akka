package server

import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.ActorSystem
import akka.actor.Props
import msgremote._
import akka.actor._
import akka.routing._
import scala.concurrent.duration.Duration
import scala.concurrent.duration._


case class GetDone

class Worker extends Actor {

  println(this) 
  def receive = {
    case mesg: String ⇒
      val moop = scala.io.Source.fromURL(mesg).mkString
        println("done " +  this )  
        sender !  GetDone
  }
}


class Joe extends Actor {
  
  var reqs: Long = _
  var counter: Long = _
  var startTimeMillis: Long = _
  val workerRouter = context.actorOf(RoundRobinPool(5).props(Props[Worker]), name = "workerRouter")
  def receive = {
    case msg: String => println("joe received " + msg + " from " + sender)
    case Message(url, reqnum) => {
       reqs = reqnum
       counter = 0 /*reset counter for now, 
       later i will create new "joe" actors for message from client */
       startTimeMillis = System.currentTimeMillis
       for (i ← 0 until reqnum) workerRouter ! url
    }
    case GetDone => {
      counter += 1
       if (counter == reqs) {
        var endTimeDiff = System.currentTimeMillis - startTimeMillis
        println(endTimeDiff/1000)
        println("!!!!!!!!!!!!!!!!!!!!!!!")
       }
    }
    case _ => println("Received unknown msg ")

  }
}


object Server extends App {

  val system = ActorSystem("GreetingSystem")
  val joe = system.actorOf(Props[Joe], name = "joe")

  println("Server ready")
}








