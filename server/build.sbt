name := "loadtest-scala-akka-client"

version := "2.3.8"

scalaVersion := "2.10.3"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-remote" % "2.3.8"
)

