package client
// http://doc.akka.io/docs/akka/snapshot/scala/microkernel.html#microkernel-scala
//https://github.com/sbt/sbt-native-packager
//https://github.com/typesafehub/config
import akka.actor._
import akka.actor.ActorDSL._
import com.typesafe.config.ConfigFactory
import msgremote._


object Greet_Sender extends App {

   println("STARTING")
   implicit val system = ActorSystem("GreetingSystem-1")
   val confing = ConfigFactory.load()

   val joe = system.actorSelection("akka.tcp://GreetingSystem@127.0.0.1:2552/user/joe")


   joe ! Message(confing.getString("woop.foo"), 1000)

}
